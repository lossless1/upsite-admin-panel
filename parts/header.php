<!DOCTYPE html>
<html>
<head>
	<title><?=$myBase['meta_t']?></title>
	<?php 
	if($myBase['meta_d'] != '' || $myBase['meta_d'] != NULL)
		echo "<meta name='description' content='{$myBase['meta_d']}'>"; 
	if($myBase['meta_k'] != '' || $myBase['meta_k'] != NULL)
		echo "<meta name='keywords' content='{$myBase['meta_k']}'>"; 
	?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?=ROOT?>assets/css/animate.css">
	<link rel="stylesheet" href="<?=ROOT?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=ROOT?>assets/css/common.css">
	<link rel="stylesheet" href="<?=ROOT?>assets/css/font-awesome.css">
	<link rel="stylesheet" href="<?=ROOT?>assets/css/docs.css">
	<link rel="stylesheet" href="<?=ROOT?>assets/css/bootstrap-social.css">
    <link rel="stylesheet" href="<?=ROOT?>assets/css/style.css">
<!--        <link rel="stylesheet" href="<?=ROOT?>assets/css/stylemenu.css">-->
    <link rel="stylesheet" href="<?=ROOT?>assets/css/owl-carousel.css">
    <link rel="stylesheet" href="<?=ROOT?>assets/css/jgallery.min.css?v=1.5.0">
    <link rel="stylesheet" href="<?=ROOT?>assets/css/lightbox.css">
<!--            <link rel="stylesheet" href="<?=ROOT?>assets/css/stylemenu.css">-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
	<script src="<?=ROOT?>assets/js/jquery.min.js"></script>
	
	
	    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery-1.11.1.min.js"></script>
     <script src="js/device.min.js"></script> <!--OPTIONAL JQUERY PLUGIN-->
    <script src="js/jquery.mb.YTPlayer.js"></script>
    <script src="js/custom.js"></script>
    <style>
	body{
		background: #000!important;
	}
        .navbar-default {
/*
    background-color: #131313;
    border-color: #e7e7e7;
*/
    background: url(/img/fon-nav.png) 50% 50% no-repeat /cover!important;
    border: none;
    border-radius: 0px;
} 

/*
.navbar-nav  {
border: 2px solid #2e2e2e!important;
    border-radius: 5px!important;
}
*/

.nav-border {
    border: 2px solid #2e2e2e!important;
    border-radius: 5px!important;
}

.navbar-default .navbar-nav > li > a {
    color: #888888!important;
    font-size: 18px!important;
}

.navbar-default .navbar-nav > li > a:hover {
    background: #171717!important;
        color: #d6c8ae!important;
    text-shadow: 0px 0px 1px rgba(217, 145, 29, 0.88)!important;
    
}



.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
    color: #d6c8ae;
    background-color: #232323;
    border-radius: 5px;
    text-shadow: 0px 0px 1px rgba(217, 145, 29, 0.88);

}
.navbar {
    margin: 0 auto;
    text-align: center;
}


.dropdown-menu > li > a:hover, 
.dropdown-menu > li > a:focus {
  
    text-decoration: none;
    background-color: #555;
        color: #d6c8ae;
    text-shadow: 0px 0px 1px rgba(217, 145, 29, 0.88);
}

.dropdown-menu {
    color: #262626;
    text-decoration: none;
    background-color: #141416;
}



.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
/*    color: #555;*/
    background-color: #141416;
            color: #d6c8ae;
    text-shadow: 0px 0px 1px rgba(217, 145, 29, 0.88);
}

.dropdown-menu .divider {
    height: 1px;
    margin: 9px 0;
    overflow: hidden;
     background-color: rgba(70, 56, 56, 0.85); 
    width: 80%;
    margin: 0 auto;

}

.navbar-nav {
margin-top: 10px;
    margin-bottom: 10px;
}

.navbar-nav > li > a {
    padding-top: 10px;
    padding-bottom: 10px;
}

.dropdown-menu > li > a {
    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: rgba(255, 255, 255, 0.76);
    white-space: nowrap;
}

.bg-ul {
    background: url(/img/menu.png) 50% 50% no-repeat /cover;
    border-radius: 5px;
}
        
        .bg-ul li {
padding: 0;
}

.bg-ul:hover {
    border-radius: 5px;
}
        
.bg-ul li:hover {
    border-radius: 5px;
}
.menu-center {
    display: flex;
    width: 100%;
    justify-content: center;
}
.bg-ul .border-right {
    border-right: 1px solid;
    border-color:#141416;
}
/*

.navbar-nav {
  width: 100%;
  text-align: center;
}

.navbar-nav > li {
  float: none;
  display: inline-block;
}

.menu-test {
    margin: 0 auto;
    height: auto;
}
*/
/*
.menu-center {
       max-width: auto;
    margin-left: 25%;
    margin-right: 25%;
}
*/
/*

    max-width: 50%;
    margin-left: 25%;
    margin-right: 25%;*/
        .pattern {
            height: 620px;
        }
		.main-menu{
			z-index: 3000;
		}
    </style>
</head>
<body>
 <script>
$(function(){
	

setTimeout(function() {$("body canvas").animate({
        opacity: 0
	}, 5000 )}, 5000);

	
$("#bg-canvas").fadeIn(8000);
setTimeout(function(){
//$("#bg-canvas img").addClass("animated shake"); //shake
}, 5000);

setTimeout(function(){
$("#bg-canvas img").addClass("animated bounceOutLeft"); //shake bounceOutLeft
$("#bg-canvas img").attr("src","<?=ROOT?>img/bg-header-4.png");
//$("#bg-canvas img").show();
$("#bg-canvas img").removeClass("bounceOutLeft");
$("#bg-canvas img").addClass("bounceInDown");
}, 6900);

setTimeout(function(){
	//$("#bg-canvas img").hide();
	
//$("#bg-canvas img").addClass("animated rotateInDownLeft"); //shake
}, 7500);
});
</script>
    
    
<header id="header" class="big-background">
   <div class="container-fluid" style="padding:0;">
        <nav class="navbar navbar-default" style="margin:0; z-index: 3000;">
            <div class="menu-center">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
<!--                    <a class="navbar-brand" href="#">Меню </a>-->
                </div>
            <ul class="nav navbar-nav navbar-left bg-ul">
                        <li><a href="#" class="border-right"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="border-right"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="border-right"><i class="fa fa-vk" aria-hidden="true"></i></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
             
                    </ul>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav bg-ul">
<!--                        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
                        <li><a href="#">Link</a></li>
                             <li><a href="#">Link</a></li>
                                  <li><a href="#">Link</a></li>
                                       <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="100" data-close-others="false">
        Account <b class="caret"></b>
    </a>
                            <ul class="dropdown-menu">
                                <li><a tabindex="-1" href="#">My Account</a></li>
                                <li class="divider"></li>
                                <li><a tabindex="-1" href="#">Change Email</a></li>
                                <li><a tabindex="-1" href="#">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a tabindex="-1" href="#">Logout</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="100" data-close-others="false">Item <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a tabindex="-1" href="#">My Account</a></li>
                                <li class="divider"></li>
                                <li><a tabindex="-1" href="#">Change Email</a></li>
                                <li><a tabindex="-1" href="#">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a tabindex="-1" href="#">Logout</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                                 <li><a href="#">Link</a></li>
                                          <li><a href="#">Link</a></li>


                    </ul>
                    <!--
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
-->
                    <!--
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
-->
                </div>
                        <ul class="nav navbar-nav navbar-right bg-ul">
                        <li><a href="#">Ру</a></li>
       
             
                    </ul>
<!--                 /.navbar-collapse -->
<!--            </div>-->
                        <!-- /.container-fluid -->
        </nav>

        </ul>

    </div>
    
	<p style="padding:0; margin: 0; position: absolute; display: none;
			top: 0; z-index: 2000;
			left: 0;" id="bg-canvas">
		<img style='' src='<?=ROOT?>img/bg-header-2.png'  />
	</p>
	
<canvas style="margin-bottom:0; padding-bottom:0;  width: 100%; height: auto; overflow: hidden;">
	<div class="container-fluid" >
	<div class="row">
	    <div class="col-sm-12">
	        <div class="test ">
	          
        <a id="bgndVideo" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=BE6IQVw24Wc&feature=youtu.bes',containment:'body',autoPlay:true, mute:true, startAt:0, opacity:1}"></a>
        
        <div class="pattern"></div> 
<!--
            <div class="big-background-container">
                    <h1 class="big-background-title">YOUTUBE</h1>
                    <div class="divider"></div>
                    <h1 id="colorize">VIDEO BACKGROUND</h1>
                    <a href="http://designmodo.com/video-background-website/" class="big-background-btn">Back to the Article</a>
            </div>                                                    
-->
 
	        </div>
	    </div>
	</div>
	
		<div class="row">
			<div class="col-xs-12">
				<h1 class="text-center">
					<a href="<?=ROOT?>"></a>
				</h1>
			</div>
<!--
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<?php include 'parts/other/search.php'; ?>
			</div>
-->
		</div>
	</div>
	</canvas>
	<?php drawBlock(6); ?>	
	
</header>
<?php drawMenu(1); ?>

<?php include 'parts/breadcrumbs.php'; ?>

