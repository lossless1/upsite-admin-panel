<footer id="footer">
	<div class="container main-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h2 class="text-center"></h2>
				<?php drawMenu(3); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<?php drawMenu(4); ?>
				<?php drawBlock(7); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<?php drawMenu(5); ?>
				<?php drawBlock(8); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<?php drawMenu(6); ?>
				<?php drawBlock(9); ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<?php drawMenu(7); ?>
				<?php drawBlock(10); ?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php drawBlock(11); ?>
			</div>
		</div>
	</div>				
</footer>

<script src="<?=ROOT?>assets/js/gl-matrix-min.js"></script>
<script src="<?=ROOT?>assets/js/index-stars.js"></script>
	
<script src="<?=ROOT?>assets/js/bootstrap.min.js"></script>
<script src="<?=ROOT?>assets/js/owl.carousel.js"></script>
<script src="<?=ROOT?>assets/js/jgallery.min.js?v=1.5.0"></script>
<script src="<?=ROOT?>assets/js/lightbox.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
</body>
</html>
