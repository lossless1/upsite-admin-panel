<div class="container-fluid">
	<div class="row">

		<?php drawBlock(1); ?>	

		<?php
		if ($myBase['leftbar']) {?>
		<div class="container">
			<div class="row box">

				<?php include 'parts/leftbar.php'; ?>
				<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 content">

					<?php drawBlock(2); ?>	
					
					<?php 
					if ($myBase['is_rewies']) {
						include 'library/rewies.php';
					}
					if (!$parent) {
						include 'library/sons.php'; 
					}
					?>

					<?=stripslashes(htmlspecialchars_decode($myBase['full_text']))?>

					<?php drawBlock(3); ?>

				</div>
			</div>
		</div>
		<?php	}else{ ?>
		<div class="container">
			<div class="row box">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content">

					<?php drawBlock(2); ?>	

					<?=stripslashes(htmlspecialchars_decode($myBase['full_text']))?>
										
					<?php drawBlock(3); ?>	

					<?php
					if ($myBase['is_rewies']) {
						include 'library/rewies.php';
					}
					include 'library/sons.php';
					?>
				</div>
			</div>
		</div>
		<?php  	}	?>

		<?php drawBlock(4); ?>	
		
	</div>
</div>