<?php
switch ($action) {
	case 'add':
	$name = new Add($db, 'products');
	$pole = [
		['title'=> 'Введите название товара', 'name' => 'name', 'value'=>$_POST['name'], 'required' => '1', 'type'=> 'input'],
		['title'=> 'Продолжить', 'type' => 'submit'],
	];
	$name->poles = $pole;
	$name->drawForm();
	$name->saveDb(1);
		break;
	case 'edit':
	if (!isset($id)) {
		$edit = new Edit($db, 'products');
	}else{
		$name = new Add($db, 'products');
		$name->readDb($id);
		$pole = [
			['title'=> 'Введите название товара', 'name' => 'name', 'value'=>$_POST['name'], 'required' => '1', 'type'=> 'input'],
			['title'=> 'Введите алиас', 'name' => 'alias', 'value'=>$_POST['alias'], 'type'=> 'input'],
			//['title'=> 'Ску/модель/код товара/ид', 'name' => 'sku', 'value'=>$_POST['sku'], 'required' => '1', 'type' => 'input'],
			['title'=> 'Выберите категорию', 'name' => 'category','value'=>$_POST['category'], 'type' => 'category', 'required' => '1'],
			['title'=> 'Подпись (Оч. краткое описание:-))', 'name' => 'signature','value'=>$_POST['signature'], 'type' => 'input'],
			['title'=> 'Добавте фото', 'name' => 'photo','value'=>$_FILES['photo'], 'type' => 'image'],
			['title'=> 'Введите описание', 'name' => 'description','value'=>$_POST['description'], 'type' => 'textarea'],
			['title'=> 'Цена', 'name' => 'price','value'=>$_POST['price'], 'type' => 'input', 'required' => '1'],
			['title'=> 'Что входит в цену', 'name' => 'to_price','value'=>$_POST['to_price'], 'type' => 'input'],
			['title'=> 'Сроки выполнения', 'name' => 'deadline','value'=>$_POST['deadline'], 'type' => 'input'],
			['title'=> 'Исполнители', 'name' => 'performers','value'=>$_POST['performers'], 'type' => 'input'],
			['title'=> 'Подарки и бонусы', 'name' => 'presents','value'=>$_POST['presents'], 'type' => 'input'],
			['title'=> 'Рекомендуемые товары <small class="text-primary">(Пропишите id нужных товаров через "/")</small>', 'name' => 'recomended','value'=>$_POST['recomended'], 'type' => 'input'],
			['title'=> 'Введите номер сортировки', 'name' => 'sort','value'=>$_POST['sort'], 'type' => 'number'],
			['title'=> 'ОК', 'type' => 'submit'],
		];
		$name->poles = $pole;
		$pole = [
			['title'=> 'Введите название товара', 'name' => 'name', 'value'=>$_POST['name'], 'required' => '1', 'type'=> 'input'],

			['title'=> 'Введите алиас', 'name' => 'alias', 'value'=>$_POST['alias'], 'type'=> 'hidden'],
			//['title'=> 'Ску/модель/код товара/ид', 'name' => 'sku', 'value'=>$_POST['sku'], 'required' => '1', 'type' => 'input'],
			['title'=> 'Подпись (Оч. краткое описание:-))', 'name' => 'signature','value'=>$_POST['signature'], 'type' => 'input'],
			['title'=> 'Введите описание', 'name' => 'description','value'=>$_POST['description'], 'type' => 'textarea'],
			['title'=> 'Что входит в цену', 'name' => 'to_price','value'=>$_POST['to_price'], 'type' => 'input'],
			['title'=> 'Сроки выполнения', 'name' => 'deadline','value'=>$_POST['deadline'], 'type' => 'input'],
			['title'=> 'Исполнители', 'name' => 'performers','value'=>$_POST['performers'], 'type' => 'input'],
			['title'=> 'Подарки и бонусы', 'name' => 'presents','value'=>$_POST['presents'], 'type' => 'input'],
			['title'=> 'ОК', 'type' => 'submit'],
		];
		$name->poles_langs = $pole;
		$name->drawForm();
		$name->saveDb(1);
	}
		break;
	case 'del':
	$del = new Del($db, 'products');
		break;
}
?>