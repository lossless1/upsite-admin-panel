<?php
$r = mysqli_query($db, "SELECT * FROM $type WHERE parent = '$id' ORDER BY sort");
while ($f = mysqli_fetch_assoc($r)) {
	$sons[] = $f;
}
if ($sons) {
	echo "<div class='posts sons post-group-flex'>";
	foreach ($sons as $s) {
		if ($myBase['is_text_prewie'] && $myBase['is_image_prewie']) {?>
			<article class="post sons_t1_i1" tabindex="0">
				<a href="<?=ROOT?>posts/<?=$s['alias']?>"><div class="post-img" style="background: url(<?=ROOT?>img/other/<?=$s['photo']?>) 50% 50%/cover no-repeat;"></div></a>
				
				<div class="post-content">
					<h4 class="text-left"><?=$s['name']?></h4>
					<?=short(strip_tags(htmlspecialchars_decode($s['description'])), 300)?><br><br>
					<a href="<?=ROOT?>posts/<?=$s['alias']?>" class="post-detail">Подробнее&nbsp;<img src="<?=ROOT?>assets/img/back.png"></a>
				</div>
			</article>
<?php
		}
		elseif ($myBase['is_text_prewie'] == 1 && !$myBase['is_image_prewie']) {?>
			<article class="post sons_t1_i0" tabindex="0">
				<a href="<?=ROOT?>posts/<?=$s['alias']?>"><div class="post-img"></div></a>
				
				<div class="post-content">
					<h4 class="text-left"><?=$s['name']?></h4>
					<?=short(strip_tags(htmlspecialchars_decode($s['description'])), 300)?><br><br>
					<a href="<?=ROOT?>posts/<?=$s['alias']?>" class="post-detail">Подробнее&nbsp;<img src="<?=ROOT?>assets/img/back.png"></a>
				</div>
			</article>
<?php
		}
		elseif (!$myBase['is_text_prewie'] && $myBase['is_image_prewie']) {?>
			<article class="post sons_t0_i1" tabindex="0">
				<a href="<?=ROOT?>posts/<?=$s['alias']?>"><div class="post-img" style="background: url(<?=ROOT?>img/other/<?=$s['photo']?>) 50% 50%/cover no-repeat;"></div></a>
				
				<div class="post-content">
					<h4 class="text-left"><?=$s['name']?></h4>
					<a href="<?=ROOT?>posts/<?=$s['alias']?>" class="post-detail">Подробнее&nbsp;<img src="<?=ROOT?>assets/img/back.png"></a>
				</div>
			</article>
<?php
		}
	}
	echo "</div>";
}
?>